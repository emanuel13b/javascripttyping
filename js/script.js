let insideText = "";
let domEl = document.getElementById("insideText");
let cursor = document.querySelector(".cursor");
let textList = [
  "I am the best!",
  "Nobody can defeat me!",
  "Here's a potato. (╯°□°）╯︵ ┻━┻)",
  "Hacking into the servers. ###h-a-c-k-i-i-i-i-i-i-i-i-i-i-i-i-i-i--i-n-g### COMPLETE / Double click YES to Ok!"
];

const WRITE_SPEED = 50;
const DELETE_SPEED = 30;
const WAIT_AFTER_WRITE = 3000;
const WAIT_AFTER_DELETE = 200;

function initWithPhrase(phrase) {
  removeCursorAnimation();
  let ce = phrase.split("");
  let i = 0;

  let write = setInterval(function() {
    insideText += ce[i];
    domEl.innerHTML = insideText;
    i += 1;

    if (i === ce.length) {
      clearInterval(write);
      addCursorAnimation();

      setTimeout(function() {
        removeCursorAnimation();
        let deleteText = setInterval(function() {
          insideText = insideText.substring(0, insideText.length - 1);
          domEl.innerHTML = insideText;

          if (insideText.length === 0) {
            clearInterval(deleteText);
            start();
          }
        }, DELETE_SPEED);
      }, WAIT_AFTER_WRITE);
    }
  }, WRITE_SPEED);
}

function start() {
  addCursorAnimation();
  setTimeout(() => {
    let id =Math.floor(Math.random() * textList.length);
    initWithPhrase(textList[id]);
}, WAIT_AFTER_DELETE);
}

start();

function addCursorAnimation(){
  cursor.classList.add("animation");
}

function removeCursorAnimation(){
  cursor.classList.remove("animation");
}